function Get-Software() {
<#
.Synopsis
	This code will all you to extract information about products installed on a computer
	Get ALL software installed regardless of whether it's 64bit or 32bit Windows:
	c:\> Get-Software -ListAllPackages

	Get a particular package on the local computer:
	c:\> Get-Software -ListPackage -PackageName "Google Chrome";

	Get output in table or whatever format just like anything else:
	c:\> Get-Software -ListPackage -PackageName "Google Chrome" | Format-Table * -AutoSize;
	- Or -
	c:\> Get-Software -ListPackage -PackageName "Google Chrome" | Out-GridView;

	Extract the version:
	c:\> Get-Software -ListPackage -PackageName "Google Chrome" | Select-Object -ExpandProperty Version;
	Shorthand
	c:\> $chromeVersion = (Get-Software -ListPackage -PackageName "Google Chrome").Vesion;
    
	Check specific software version locally installed, available Versions on the internet, 
	and get a direct link to the installation package:
	c:\> Get-Software -Check VLC;
.NOTES
	Author  : Richard Abel - rabel001@gitlab.com & rabeljr@github.com (least used)
	Date    : 3/27/2021
	Vers    : 2.0
	URL     : https://gitlab.com/rabel001
	URL 2   : https://rabel001.gitlab.io
.DESCRIPTION
	Check specific software version locally installed, available Versions on the internet, 
	and get a direct link to the installation package
.LINK
	https://gitlab.com/powershell-scripts1/use-runas.ps1/-/blob/master/Get-Software.v2.ps1
.EXAMPLE
	Get-Software -ListAllPackages
	Get ALL software installed regardless of whether it's 64bit or 32bit Window
.EXAMPLE
	Get-Software -ListPackage -PackageName "Google Chrome"
	Get a particular package on the local computer:
.EXAMPLE
	Get-Software -ListPackage -PackageName "Google Chrome" | Select-Object -ExpandProperty Version
	Extract the version
.EXAMPLE
	Get-Software -Check VLC
	Check specific software version locally installed, available Versions on the internet, 
	and get a direct link to the installation package:
#>
	[CmdletBinding(PositionalBinding=$true)]
	param(
		[parameter(Position=0, Mandatory=$false, ParameterSetName="WhithListAllPackages")]
		[Switch]$ListAllPackages,
		[parameter(Position=0, Mandatory=$false, ParameterSetName="WithListPackage")]
		[Switch]$ListPackage,
		[parameter(Position=0, Mandatory=$false, ParameterSetName="WithWebRequest")]
		[ValidateNotNullOrEmpty()]
		[String]$WebRequest,
		[parameter(Position=1, Mandatory=$true, ParameterSetName="WithListPackage")]
		[ValidateNotNullOrEmpty()]
		[String]$PackageName,
		[parameter(Position=0, Mandatory=$false, ParameterSetName="WithCheck")]
		[ValidateSet(
			"AutoHotKey",
			"Calibre",
			"Calibre2",
			"CCEnhancer",
			"CCleaner",
			"Flash",
			"FoxIt",
			"GIMP",
			"KeePass",
			"MPC_HC",
			"NextCloud",
			"OpenVPN",
			"PaintDotNet",
			"SCite4ahk",
			"VirtualBox",
			"VLC",
			"VSDC"
		)]
		[String]$Check
	)
	$CurrentPath = $PSScriptRoot;
	$LogFile = @{};
	Set-Variable -Name "tab" -value $(" "*5) -Scope Local;

	function WebRequest() {
		param([string]$url, [string]$method)
		if([string]::IsNullOrEmpty($method) -eq $false) {
			Write-Host "`n$(" "*5)Is Method Empty?`n$(" "*5)$([string]::IsNullOrEmpty($method))`n" -ForegroundColor Yellow;
			$w = [System.Net.WebRequest]::Create($url);
			return $w.GetResponse().ResponseUri.AbsoluteUri;
		} else {
			#Used to ignode SSL certifate error:
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
			#Used to set acceptabble ciphers in certificates
			[System.Net.ServicePointManager]::SecurityProtocol =  @("Ssl3","Tls","Tls11","Tls12");
			$w = new-object System.Net.WebClient;
			$w.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36");
			$content = $w.DownloadString($url);
			return $content;
		}
	}
	function ListAllPackages() {
		$packages = gci HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall | %{Get-ItemProperty $_.pspath}
		$packages += gci HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall | %{Get-ItemProperty $_.pspath}
		return $packages;
	}
	function ListPackage() {
		param(
			[parameter(Mandatory=$true, ParameterSetName="WithStarts")]
			[string]$StartsWith,
			[parameter(Mandatory=$true, ParameterSetName="WithEnds")]
			[string]$EndsWith,
			[parameter(Mandatory=$true, ParameterSetName="WithContains")]
			[string]$Contains
		)
		if($StartsWith) {
			return ListAllPackages | ?{$_.DisplayName -match "^$($StartsWith)"};
		} elseif($EndsWith) {
			return ListAllPackages | ?{$_.DisplayName -match "$($EndsWith)$"};
		} elseif($Contains) {
			return ListAllPackages | ?{$_.DisplayName -match $Contains};
		}
	}
	#function Build-Hash($webVersion, $localVersion, $downloadLink) {
	#	return @{
	#		"Web Version" = $webVersion;
	#		"Local Version" = $localVersion;
	#		"Download Link" = $downloadLink;
	#	}
	#}
	function print([string]$webVersion, [string]$localVersion, [string]$downloadLink, [string]$downloadLink64) {
		#Write-Host "webVersion: $($webVersion), localVersion: $($localVersion), downloadLink: $($downloadLink), downloadLink64: $($downloadLink64)";
		Write-Host "";
		if($webVersion) {
			Write-Host "$(" "*5)Web Version:`t$($webVersion)" -ForegroundColor Green;
		}
		if($localVersion) {
			Write-Host "$(" "*5)Local Version:`t$($localVersion)" -ForegroundColor Green;
		}
		if($downloadLink) {
			Write-Host "$(" "*5)Download Link:`t$($downloadLink)" -ForegroundColor Green;
		}
		if($downloadLink64) {
			Write-Host "$(" "*5)Download Link:`t$($downloadLink64)" -ForegroundColor Green;
		}
		#$hashtable.GetEnumerator() | %{
		#	Write-Host "$(" "*5)$($_.key):$(" "*$(20 - $_.key.Length))$($_.value)" -ForegroundColor Green;
		#}
		Write-Host "";
	}
	if($WebRequest) {
		WebRequest $WebRequest;
	} elseif($ListAllPackages) {
		return ListAllPackages;
	} elseif($ListPackage) {
		$pkg = ListPackage -Contains "$($PackageName)";
		return $pkg;
	} elseif($Check) {
		switch($Check) {
			"AutoHotKey" {
				$url = "https://autohotkey.com/download/1.1/version.txt";
				$content = WebRequest $url;
				$webVersion = $content;
				$pkg = ListPackage -StartsWith "autohotkey";
				$downloadLink = "https://autohotkey.com/download/ahk-install.exe";
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
				#print @{
				#	"Web Version" = $webVersion;
				#	"Local Version" = $localVersion;
				#	"Download Link" = $downloadLink;
				#}
			}
			"Calibre" {
				$url = "https://calibre-ebook.com/download_windows";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "calibre";
				$groups = [regex]::Match($content, '"download_block[^h]+href="(?<download>[^"]+)">[\s\S]*Version:\s(?<version>.*?)\s', 'MultiLine').Captures[0].Groups;
				$webVersion = $groups['version'].Value;
				$shortLink = $groups['download'].Value;
				$downloadLink = "https://calibre-ebook.com$($shortLink)";
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"Calibre2" {
				$url = "https://calibre-ebook.com/download_windows";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "calibre";
				$groups = [regex]::Match($content, '"download_block[^h]+href="(?<download>[^"]+)">[\s\S]*Version:\s(?<version>.*?)\s', 'MultiLine').Captures[0].Groups;
				$webVersion = $groups['version'].Value;
				$shortLink = $groups['download'].Value;
				$downloadLink = "https://calibre-ebook.com$($shortLink)";
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"CCEnhancer" {
				$url = "https://singularlabs.com/software/ccenhancer/download-ccenhancer/"
				$content = WebRequest $url;
				$pkg = ListPackage -StartsWith "CCEnhancer";
				$downloadLink = [regex]::Match($content, '<a class="aligncenter download-button" href="(?<download>[^"]+)"').Groups["download"].Value;
				$webVersion = [regex]::Match($content, "<td>(?<version>\d+\.\d+\.\d+)<\/td>").Captures[0].Groups["version"].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
				#print @{
				#	"Web Version" = $webVersion;
				#	"Local Version" = $pkg.DisplayVersion;
				#	"Download Link" = $downloadLink;
				#}				
			}
			"CCleaner" {
				$url = "https://www.ccleaner.com/ccleaner/version-history";
				$content = WebRequest $url;
				$pkg = ListPackage -Contains "ccleaner";
				$webVersion = ([regex]'<h6>(?<version>[^\s]+)\s.*?<\/h6>').Matches($content).Captures[0].Groups['version'].Value.Replace('v', '');
				$downloadUrl = "https://www.ccleaner.com/ccleaner/download/standard";
				$dlContent = WebRequest($downloadUrl);
				$setupExe = [regex]::Match($dlContent, '"(?<version>[^"]+\.exe)"').Captures[0].Groups["version"].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $setupExe;
				#print @{
				#	"Web Version" = $webVersion;
				#	"Local Version" = $pkg.DisplayVersion;
				#	"Download Link" = $setupExe;
				#}
			}
			"Flash" {
				$xml_url = "https://fpdownload.macromedia.com/pub/flashplayer/masterversion/masterversion.xml";
				try
				{
				    $xml_versions = New-Object XML;
				    $xml_versions.Load($xml_url);
				}
				catch [System.Net.WebException]
				{
				    Write-Warning "Failed to access $xml_url";
				    $empty_line | Out-String;
				    $xml_text = "Please consider running this script again. Sometimes the XML-file just isn't queryable for no apparent reason. The success rate 'in the second go' usually seems to be a bit higher.";
				    Write-Output $xml_text;
				    $empty_line | Out-String;
				    Return "Exiting without checking the latest Flash version numbers or without updating Flash.";
				}
				$xml_activex_win8_current = ($xml_versions.version.release.ActiveX_win8.version).replace(",",".");
				$xml_activex_win10_current = ($xml_versions.version.release.ActiveX_win10.version).replace(",",".");
				$xml_activex_edge_current = ($xml_versions.version.release.ActiveX_Edge.version).replace(",",".");
				$xml_activex_win_current = ($xml_versions.version.release.ActiveX_win.version).replace(",",".");
				$xml_plugin_win_current = ($xml_versions.version.release.NPAPI_win.version).replace(",",".");
				$xml_plugin_mac_current = ($xml_versions.version.release.NPAPI_mac.version).replace(",",".");
				$xml_plugin_linux_current = ($xml_versions.version.release.NPAPI_linux.version).replace(",",".");
				$xml_ppapi_win_current = ($xml_versions.version.release.PPAPI_win.version).replace(",",".");
				$xml_ppapi_winchrome_current = ($xml_versions.version.release.PPAPI_winchrome.version).replace(",",".");
				$xml_ppapi_mac_current = ($xml_versions.version.release.PPAPI_mac.version).replace(",",".");
				$xml_ppapi_macchrome_current = ($xml_versions.version.release.PPAPI_macchrome.version).replace(",",".");
				$xml_ppapi_linux_current = ($xml_versions.version.release.PPAPI_linux.version).replace(",",".");
				$xml_ppapi_linuxchrome_current = ($xml_versions.version.release.PPAPI_linuxchrome.version).replace(",",".");
				$xml_ppapi_chromeos_current = ($xml_versions.version.release.PPAPI_chromeos.version).replace(",",".");
				$pkg = $GetSoftware.ListPackage("", "", "adobe flash")
				Write-Host "";
				Write-Host "$(" "*5)XML Version:`t$($xml_activex_win_current)" -ForegroundColor Green;
				Write-Host "$(" "*5)Local Version:`t$($pkg.DisplayVersion)`b" -ForegroundColor Green;
				Write-Host "";
			}
			"FoxIt" {
				$url = "https://www.filehorse.com/download-foxit-reader/";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "foxit";
				$webVersion = [regex]::Match($content, "Foxit Reader (?<version>\d+[^<]+)<", "MultiLine, IgnoreCase").Captures[0].Groups["version"].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion;
			}
			"GIMP" {
				$url = "needs more work";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "gimp";
				$downloadLink = ([regex]'"[^"]+\.exe"').Match($content).Value.Replace('"', '');
				$webVersion = ([regex]'-(?<version>\d+.*?)-').Match($downloadLink).Captures[0].Groups["version"].Value
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"KeePass" {
				$url = "https://keepass.info/download.html"
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "keepass";
				$downloadLink = [regex]::Match($content, "[^>]+['`"](?<download>[^>]+\.exe[^'`"]+)['`"][^>]+>").Captures[0].Groups["download"].Value;
				$webVersion = [regex]::Match($content, ">KeePass\s+(?<version>\d+\.\d+)<").Captures[0].Groups["version"].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"MPC_HC" {
				$url = "https://github.com/clsid2/mpc-hc/releases";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "mpc-hc";
				$downloadLink = "https://github.com" + [regex]::Match($content, '"(?<download>[^"]+\.exe)').Groups["download"].Value;
				$webVersion = [regex]::Match($downloadLink, "\d+\.\d+\.\d+").Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"NextCloud" {
				$url = "https://nextcloud.com/install/?pk_campaign=clientupdate#install-clients";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "nextcloud";
				$downloadLink = ([regex]'"[^"]+\.msi"').Match($content).Value.Replace('"', '');
				$webVersion = ([regex]'-(?<version>\d+.*?)-').Match($downloadLink).Captures[0].Groups["version"].Value
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"OpenVPN" {
				$url = "https://openvpn.net/community-downloads/";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "openvpn";
				$webVersion = [regex]::Matches($content, "<span>openvpn\s(?<version>\d[^\s]+)\s", 'IgnoreCase').Captures[0].Groups["version"].Value;
				if($pkg.InstallLocation -match "86") {
					$downloadLink = [regex]::Matches($content, '"(?<32>[^"]+[^4]\.exe)"').Captures[0].Groups["32"].Value;
				} else {
					$downloadLink = [regex]::Matches($content, '"(?<64>[^"]+64\.exe)"').Captures[0].Groups["64"].Value;
				}
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"PaintDotNet" {
				$url = "https://www.getpaint.net/download.html";
				$content = WebRequest($url);
				$downloadLink = [regex]::Matches($content, 'href="(?<dl>.*?downloads[^"]+)"').Captures[0].Groups["dl"].Value;
				$pkg = ListPackage -Contains "paint";
				$downloadPage = WebRequest($downloadLink);
				#$downloadPage;
				$webVersion = ([regex]"(?<version>\d+\.\d+\.\d+)").Matches($downloadPage).Captures[0].Groups["version"].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"SCite4ahk" {
				$url = "https://github.com/fincs/SciTE4AutoHotkey/releases";
				$content = WebRequest($url);
				$pkg = ListPackage -StartsWith "scite4";
				$webVersion = [regex]::Matches($content, '\/SciTE4AutoHotkey\/releases\/tag\/(?<version>[^"]+)"').Captures[0].Groups["version"].Value;
				$downloadLink = "http://fincs.ahk4.net/scite4ahk/dl/s4ahk-install.exe";
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"VirtualBox" {
				$url = "https://www.virtualbox.org/wiki/Downloads"
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "virtualbox";
				$downloadLink = [regex]::Match($content, '"(?<download>[^"]+\.exe)').Groups["download"].Value;
				$webVersion = [regex]::Match($content, '"[^"]+\/(?<version>\d*\.\d*\.\d*)\/[^"]+\.exe').Groups["version"].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"VLC" {
				$url = "https://www.videolan.org/vlc/download-windows.html";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "vlc";
				$webVersion = [regex]::Match($content, "downloadVersion'[^<]+(?:\n|\n\r)\s+(?<version>[0-9,.]+)<").Captures[0].Groups["version"].Value;
				$downloadLink = ([regex]::Match($content, '<a\shref="([^"]+\.exe)">')).Captures.Groups[1].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $downloadLink;
			}
			"VSDC" {
				$url = "http://www.videosoftdev.com/free-video-editor/download";
				$content = WebRequest($url);
				$pkg = ListPackage -Contains "vsdc";
				$webVersion = [regex]::Match($content, "(<strong>(?<version>[^<]+)<)", "Multiline").Captures[0].Groups["version"].Value;
				#[regex]::Match($content, "<strong>(?<version>\d+\.\d+\.\d+)<\/strong>", "Multiline").Captures[0].Groups["version"].Value;
				$groups = [regex]::Matches($content, "href=.(?<download>\/services\/download\.aspx\?[^`"]+)[^63]+(?<type>(6|3)[^<]+)<", "Multiline");
				$x64downloadLink = "http://www.videosoftdev.com$($groups.Captures[0].Groups["download"].Value)";
				$exe64 = $GetSoftware.WebRequest($x64downloadLink, "HEAD");
				$x32downloadLink = "http://www.videosoftdev.com$($groups.Captures[1].Groups["download"].Value)";
				$exe32 = $GetSoftware.WebRequest($x32downloadLink, "HEAD");
				$shortLink = $groups['download'].Value;
				print -webVersion $webVersion -localVersion $pkg.DisplayVersion -downloadLink $exe32 -downloadLink64 $exe64;
			}
		}
	}
}
