<# 
	A work in progress.
	Created by: rabel001@gmail.com$
	Date: 10/24/2019
	Usage:
		1. $GetSoftware.ListOfChecks() for a list of available software to search.
		2. $GetSoftware.Check.VLC() (or whatever is available in the ListOfChecks)
       		    will show installed version and available version with a link to 
			    the latest update.
		3. $GetSoftware.ListPackages() will show all packages installed on the system.
		4. $GetSoftware.ListPackage("nameofpackage") will give you an individual package.
#>
#$ScriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path;
#[void][System.Reflection.Assembly]::LoadFile("$($PSScriptRoot)\lib\Software\Software.dll");
#[System.Reflection.Assembly]::LoadFile("$($PSScriptRoot)\lib\CsQuery.1.3.4\lib\net40\CsQuery.dll")

Set-Variable -Name "GetSoftware" -Value @{} -Scope global;
$GetSoftware.CurrentPath = $PSScriptRoot;
#$GetSoftware.Libraries = "$($PSScriptRoot)\lib";
#$GetSoftware.DLL = "$($GetSoftware.Libraries)\Software\Software.dll";
#[void][System.Reflection.Assembly]::LoadFile($GetSoftware.DLL);

$GetSoftware.LogFile = @{};
$GetSoftware.LogFile | Add-Member -Name "Write" -MemberType ScriptMethod -Value {
    param([string]$str)
    Out-File -FilePath $GetSoftware.LogFile.FileName -InputObject $str -Append;
    Write-Host "'$($str)' was logged to $($GetSoftware.LogFile.FileName)";
}

$GetSoftware | Add-Member -Name "CurrentTime" -MemberType ScriptMethod -Value {
    $now = Get-Date;
    $currentMinute = "{0:00}" -f ($now.Minute + 2);
    $currentHour = "{0:00}" -f $now.Hour;
    $currentDay = "{0:00}" -f $now.Day;
    $currentMonth = "{0:00}" -f $now.Month;
    $currentYear = $now.Year;
    $GetSoftware.date = "$($currentMonth)$($currentDay)$($currentYear)";
    $GetSoftware.time = "$($currentHour)$($currentMinute)";
    $GetSoftware.dateTime = "$($GetSoftware.date)_$($GetSoftware.time)";
}
$GetSoftware.CurrentTime();
$GetSoftware.LogFile.FileName = "$($GetSoftware.currentPath)\$GetSoftware_$($GetSoftware.dateTime)_.log";

$GetSoftware | Add-Member -Name "ListPackages" -MemberType ScriptMethod -Value {
	#return [Software.Installed]::GetAll();
	$software = gci HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall | %{Get-ItemProperty $_.pspath}
	$software += gci HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall | %{Get-ItemProperty $_.pspath}
	return $software;
}

#$GetSoftware.psobject.Methods
$GetSoftware | Add-Member -Name "ListPackage" -MemberType ScriptMethod -Value {
	#param([string]$pkg)
	#return [Software.Installed]::GetPackage($pkg);
	param(
		[parameter(Mandatory=$true, ParameterSetName="WithStarts")]
		[string]$StartsWith,
		[parameter(Mandatory=$true, ParameterSetName="WithEnds")]
		[string]$EndsWith,
		[parameter(Mandatory=$true, ParameterSetName="WithContains")]
		[string]$Contains
	)
	$software = $GetSoftware.ListPackages();
	if($StartsWith) {
		return $software | ?{$_.DisplayName -match "^$($StartsWith)"};
	} elseif($EndsWith) {
		return $software | ?{$_.DisplayName -match "$($EndsWith)$"};
	} elseif($Contains) {
		return $software | ?{$_.DisplayName -match $Contains};
	}
}

$GetSoftware | Add-Member -Name "WebRequest" -MemberType ScriptMethod -Value {
	param([string]$url, [string]$method)
	if([string]::IsNullOrEmpty($method) -eq $false) {
		$w = [System.Net.WebRequest]::Create($url);
		return $w.GetResponse().ResponseUri.AbsoluteUri;
	} else {
		#Used to ignode SSL certifate error:
		[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
		#Used to set acceptabble ciphers in certificates
		[System.Net.ServicePointManager]::SecurityProtocol =  @("Ssl3","Tls","Tls11","Tls12");
		$w = new-object System.Net.WebClient;
		$w.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0 Waterfox/56.2.14");
		$content = $w.DownloadString($url);
		return $content;
	}
}
#$members = new-object System.Collections.ObjectModel.Collection[System.Management.Automation.PSMemberInfo]
$GetSoftware.Check2 = @{}; # New-Object Management.Automation.PSObject;
$GetSoftware.Check = [pscustomobject]@{};
$GetSoftware.Check | Add-Member -Name "Flash" -MemberType ScriptMethod -Value {
	$xml_url = "https://fpdownload.macromedia.com/pub/flashplayer/masterversion/masterversion.xml";
	try
	{
	    $xml_versions = New-Object XML;
	    $xml_versions.Load($xml_url);
	}
	catch [System.Net.WebException]
	{
	    Write-Warning "Failed to access $xml_url";
	    $empty_line | Out-String;
	    $xml_text = "Please consider running this script again. Sometimes the XML-file just isn't queryable for no apparent reason. The success rate 'in the second go' usually seems to be a bit higher.";
	    Write-Output $xml_text;
	    $empty_line | Out-String;
	    Return "Exiting without checking the latest Flash version numbers or without updating Flash.";
	}
	$xml_activex_win8_current = ($xml_versions.version.release.ActiveX_win8.version).replace(",",".");
	$xml_activex_win10_current = ($xml_versions.version.release.ActiveX_win10.version).replace(",",".");
	$xml_activex_edge_current = ($xml_versions.version.release.ActiveX_Edge.version).replace(",",".");
	$xml_activex_win_current = ($xml_versions.version.release.ActiveX_win.version).replace(",",".");
	$xml_plugin_win_current = ($xml_versions.version.release.NPAPI_win.version).replace(",",".");
	$xml_plugin_mac_current = ($xml_versions.version.release.NPAPI_mac.version).replace(",",".");
	$xml_plugin_linux_current = ($xml_versions.version.release.NPAPI_linux.version).replace(",",".");
	$xml_ppapi_win_current = ($xml_versions.version.release.PPAPI_win.version).replace(",",".");
	$xml_ppapi_winchrome_current = ($xml_versions.version.release.PPAPI_winchrome.version).replace(",",".");
	$xml_ppapi_mac_current = ($xml_versions.version.release.PPAPI_mac.version).replace(",",".");
	$xml_ppapi_macchrome_current = ($xml_versions.version.release.PPAPI_macchrome.version).replace(",",".");
	$xml_ppapi_linux_current = ($xml_versions.version.release.PPAPI_linux.version).replace(",",".");
	$xml_ppapi_linuxchrome_current = ($xml_versions.version.release.PPAPI_linuxchrome.version).replace(",",".");
	$xml_ppapi_chromeos_current = ($xml_versions.version.release.PPAPI_chromeos.version).replace(",",".");
	$pkg = $GetSoftware.ListPackage("", "", "adobe flash")
	Write-Host "`nXML Version:`t`t$($xml_activex_win_current)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)`b" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "CCleaner" -MemberType ScriptMethod -Value {
	$url = "https://www.ccleaner.com/ccleaner/version-history";
	$content = $GetSoftware.WebRequest($url);
	#$pkg = $GetSoftware.ListPackage("ccleaner");
	$pkg = $GetSoftware.ListPackage("", "", "ccleaner");
	$webVersion = ([regex]'<h6>(?<version>[^\s]+)\s.*?<\/h6>').Matches($content).Captures[0].Groups['version'].Value.Replace('v', '');
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	$downloadUrl = "https://www.ccleaner.com/ccleaner/download/standard";
	$dlContent = $GetSoftware.WebRequest($downloadUrl);
	$setupExe = [regex]::Match($dlContent, '"(?<version>[^"]+\.exe)"').Captures[0].Groups["version"].Value;
	Write-Host "Download Link:`t`t$($setupExe)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member "OpenVPN" -MemberType ScriptMethod -Value {
	$url = "https://openvpn.net/community-downloads/";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "openvpn");
	$webVersion = [regex]::Matches($content, "<span>openvpn\s(?<version>\d[^\s]+)\s", 'IgnoreCase').Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	if($pkg.InstallLocation -match "86") {
		$downloadLink = [regex]::Matches($content, '"(?<32>[^"]+[^4]\.exe)"').Captures[0].Groups["32"].Value;
		Write-Host "Download Linke:`t`t$($downloadLink)" -ForegroundColor Green;
	} else {
		$downloadLink = [regex]::Matches($content, '"(?<64>[^"]+64\.exe)"').Captures[0].Groups["64"].Value;
		Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
	}
}

$GetSoftware.Check | Add-Member "VLC" -MemberType ScriptMethod -Value {
	$url = "https://www.videolan.org/vlc/download-windows.html";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "vlc");
	$webVersion = [regex]::Match($content, "downloadVersion'[^<]+(?:\n|\n\r)\s+(?<version>[0-9,.]+)<").Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	$downloadLink = ([regex]::Match($content, '<a\shref="([^"]+\.exe)">')).Captures.Groups[1].Value;
	Write-Host "Download Link:`t`thttps:$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member "MCP_MediaPlayerClassic" -MemberType ScriptMethod -Value {
	$url = "https://mpc-hc.org/downloads/";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "mpc");
	$installers = ([regex]'("[^"]+|=[^=]+)\.exe').Matches($content).ForEach({$_.Value.Split("=")[1]});
	$x32downloadLink = $installers[0];
	$x64downloadLink = $installers[1];
	$webVersion = ([regex]'_v(?<version>\d+.*?)_').Match($x64installer).Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "x64 Download Link:`t$($x64downloadLink)" -ForegroundColor Green;
	Write-Host "x32 Download Link:`t$($x32downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member "AutoHotKey" -MemberType ScriptMethod -Value {
	$url = "https://autohotkey.com/download/1.1/version.txt";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("autohotkey", "", "");
	Write-Host "`nWeb Version:`t`t$($content)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	$downloadLink = "https://autohotkey.com/download/ahk-install.exe";
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member "SCite4ahk" -MemberType ScriptMethod -Value {
	$url = "https://github.com/fincs/SciTE4AutoHotkey/releases";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("scite4", "", "");
	$webVersion = [regex]::Matches($content, '\/SciTE4AutoHotkey\/releases\/tag\/(?<version>[^"]+)"').Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	$downloadLink = "http://fincs.ahk4.net/scite4ahk/dl/s4ahk-install.exe";
	Write-Host "Download Link:`t`t$($downloadLink)`n";
}

$GetSoftware.Check | Add-Member "PaintDotNet" -MemberType ScriptMethod -Value {
	$url = "https://www.getpaint.net/download.html";
	$content = $GetSoftware.WebRequest($url);
	$downloadLink = [regex]::Matches($content, 'href="(?<dl>.*?downloads[^"]+)"').Captures[0].Groups["dl"].Value;
	$pkg = $GetSoftware.ListPackage("", "", "paint");
	$downloadPage = $GetSoftware.WebRequest($downloadLink);
	#$downloadPage;
	$webVersion = ([regex]"(?<version>\d+\.\d+\.\d+)").Matches($downloadPage).Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}
$GetSoftware.Check2.psobject.Members.add([PSScriptMethod]::new("TestThis", { return "this"; }))
$GetSoftware.Check.psobject.Members.add([PSScriptMethod]::new("Calibre2", {
		$url = "https://calibre-ebook.com/download_windows";
		$content = $GetSoftware.WebRequest($url);
		$pkg = $GetSoftware.ListPackage("", "", "calibre");
		$groups = [regex]::Match($content, '"download_block[^h]+href="(?<download>[^"]+)">[\s\S]*Version:\s(?<version>.*?)\s', 'MultiLine').Captures[0].Groups;
		$webVersion = $groups['version'].Value;
		Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
		Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
		$shortLink = $groups['download'].Value;
		$downloadLink = "https://calibre-ebook.com$($shortLink)";
		Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
	})
);

$GetSoftware.Check | Add-Member -Name "Calibre" -MemberType ScriptMethod -Value {
	$url = "https://calibre-ebook.com/download_windows";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "calibre");
	$groups = [regex]::Match($content, '"download_block[^h]+href="(?<download>[^"]+)">[\s\S]*Version:\s(?<version>.*?)\s', 'MultiLine').Captures[0].Groups;
	$webVersion = $groups['version'].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	$shortLink = $groups['download'].Value;
	$downloadLink = "https://calibre-ebook.com$($shortLink)";
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}
#$object = new-object Management.Automation.PSObject;
#$script1 = [scriptBlock] { param($x,$y); Write-Host "Method1 X: $x Y: $y"; $x+$y}
#$object.psobject.members.Add($member1)
<#
$members = new-object System.Collections.ObjectModel.Collection[System.Management.Automation.PSMemberInfo]
$members.Add([psscriptmethod]::new("Calibre", { 
		$url = "https://calibre-ebook.com/download_windows";
		$content = $GetSoftware.WebRequest($url);
		$pkg = $GetSoftware.ListPackage("calibre");
		$groups = [regex]::Match($content, '"download_block[^h]+href="(?<download>[^"]+)">[\s\S]*Version:\s(?<version>.*?)\s', 'MultiLine').Captures[0].Groups;
		$webVersion = $groups['version'].Value;
		Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
		Write-Host "Local Version:`t`t$($pkg.Version)" -ForegroundColor Green;
		$shortLink = $groups['download'].Value;
		$downloadLink = "https://calibre-ebook.com$($shortLink)";
		Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
	})
);

$GetSoftware.Check2 = Add-Member -MemberType MemberSet -Name Check -Value $b -passthru
#>
$GetSoftware.Check | Add-Member "FoxIt" -MemberType ScriptMethod -Value {
	$url = "https://www.filehorse.com/download-foxit-reader/";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "foxit");
	$webVersion = [regex]::Match($content, "Foxit Reader (?<version>\d+[^<]+)<", "MultiLine, IgnoreCase").Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member "VSDC" -MemberType ScriptMethod -Value {
	$url = "http://www.videosoftdev.com/free-video-editor/download";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "vsdc");
	$webVersion = [regex]::Match($content, "(<strong>(?<version>[^<]+)<)", "Multiline").Captures[0].Groups["version"].Value;
	#[regex]::Match($content, "<strong>(?<version>\d+\.\d+\.\d+)<\/strong>", "Multiline").Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	$groups = [regex]::Matches($content, "href=.(?<download>\/services\/download\.aspx\?[^`"]+)[^63]+(?<type>(6|3)[^<]+)<", "Multiline");
	$x64downloadLink = "http://www.videosoftdev.com$($groups.Captures[0].Groups["download"].Value)";
	$exe64 = $GetSoftware.WebRequest($x64downloadLink, "HEAD");
	$x32downloadLink = "http://www.videosoftdev.com$($groups.Captures[1].Groups["download"].Value)";
	$exe32 = $GetSoftware.WebRequest($x32downloadLink, "HEAD");
	$shortLink = $groups['download'].Value;
	Write-Host "x64 Download Link:`t$($exe64)" -ForegroundColor Green;
	Write-Host "x32 Download Link:`t$($exe32)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "NextCloud" -MemberType ScriptMethod -Value {
	$url = "https://nextcloud.com/install/?pk_campaign=clientupdate#install-clients";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "nextcloud");
	$downloadLink = ([regex]'"[^"]+\.exe"').Match($content).Value.Replace('"', '');
	$webVersion = ([regex]'-(?<version>\d+.*?)-').Match($downloadLink).Captures[0].Groups["version"].Value
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "GIMP" -MemberType ScriptMethod -Value {
	$url = "needs more work";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "gimp");
	$downloadLink = ([regex]'"[^"]+\.exe"').Match($content).Value.Replace('"', '');
	$webVersion = ([regex]'-(?<version>\d+.*?)-').Match($downloadLink).Captures[0].Groups["version"].Value
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "KeePass" -MemberType ScriptMethod -Value {
	$url = "https://keepass.info/download.html"
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "keepass");
	$downloadLink = [regex]::Match($content, "[^>]+['`"](?<download>[^>]+\.exe[^'`"]+)['`"][^>]+>").Captures[0].Groups["download"].Value;
	$webVersion = [regex]::Match($content, ">KeePass\s+(?<version>\d+\.\d+)<").Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "CCEnhancer" -MemberType ScriptMethod -Value {
	$url = "https://singularlabs.com/software/ccenhancer/download-ccenhancer/"
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("CCEnhancer");
	$downloadLink = [regex]::Match($content, '<a class="aligncenter download-button" href="(?<download>[^"]+)"').Groups["download"].Value;
	$webVersion = [regex]::Match($content, "<td>(?<version>\d+\.\d+\.\d+)<\/td>").Captures[0].Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "VirtualBox" -MemberType ScriptMethod -Value {
	$url = "https://www.virtualbox.org/wiki/Downloads"
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "virtualbox");
	$downloadLink = [regex]::Match($content, '"(?<download>[^"]+\.exe)').Groups["download"].Value;
	$webVersion = [regex]::Match($content, '"[^"]+\/(?<version>\d*\.\d*\.\d*)\/[^"]+\.exe').Groups["version"].Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "MPC_HC" -MemberType ScriptMethod -Value {
	$url = "https://github.com/clsid2/mpc-hc/releases";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "mpc-hc");
	$downloadLink = "https://github.com" + [regex]::Match($content, '"(?<download>[^"]+\.exe)').Groups["download"].Value;
	$webVersion = [regex]::Match($downloadLink, "\d+\.\d+\.\d+").Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}

$GetSoftware.Check | Add-Member -Name "Everything" -MemberType ScriptMethod -Value {
	$url = "https://www.voidtools.com/downloads/";
	$content = $GetSoftware.WebRequest($url);
	$pkg = $GetSoftware.ListPackage("", "", "everything");
	$downloadLink = "https://www.voidtools.com/" + [regex]::Match($content, '"(?<download>[^"]+64[^\.]*\.exe)').Groups["download"].Value;
	$webVersion = [regex]::Match($downloadLink, "\d+\.\d+\.\d+").Value;
	Write-Host "`nWeb Version:`t`t$($webVersion)" -ForegroundColor Green;
	Write-Host "Local Version:`t`t$($pkg.DisplayVersion)" -ForegroundColor Green;
	Write-Host "Download Link:`t`t$($downloadLink)`n" -ForegroundColor Green;
}


$GetSoftware | Add-Member -MemberType NoteProperty -Name "ListOfChecks" -Value $($GetSoftware.Check | Get-Member -MemberType ScriptMethod | Select-Object Name, MemberType, Definition);
